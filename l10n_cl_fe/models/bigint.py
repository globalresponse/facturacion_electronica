# -*- coding: utf-8 -*-
from flectra import fields


class BigInt(fields.Integer):
    column_type = ('int8', 'bigint')
